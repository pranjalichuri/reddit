package com.reddit.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.reddit.utils.ImageUtils;
import com.reddit.utils.ParcelableUtil;

import static com.reddit.utils.Utils.parseDate;


public class UserInfo extends BaseModel implements Parcelable {

    public static final String EMAIL_ID = "email";
    public static final String CONTACT_1_KEY = "contact1";
    public static final String CONTACT_2_KEY = "contact2";
    public static final String DOB = "date_of_birth";
    public static final String GENDER = "gender";
    public static final String AVATAR = "avatar";

    @Expose
    @SerializedName("avatar_url")
    public String avatar;
    @Expose
    @SerializedName(DOB)
    public String dateOfBirth;
    @Expose
    @SerializedName(GENDER)
    public String gender;
    @Expose
    @SerializedName(CONTACT_1_KEY)
    public String contact1;
    @Expose
    @SerializedName(CONTACT_2_KEY)
    public String contact2;
    @Expose
    @SerializedName("last_name")
    public String lastName;
    @Expose
    @SerializedName("first_name")
    public String firstName;
    @Expose
    @SerializedName("email")
    public String email;

    public UserInfo() {
    }

    public UserInfo(@NonNull UserInfo userInfo) {
        this(ParcelableUtil.unMarshall(ParcelableUtil.marshall(userInfo)));
    }

    protected UserInfo(Parcel in) {
        super(in);
        avatar = in.readString();
        dateOfBirth = in.readString();
        gender = in.readString();
        contact1 = in.readString();
        contact2 = in.readString();
        lastName = in.readString();
        firstName = in.readString();
        email = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(avatar);
        dest.writeString(dateOfBirth);
        dest.writeString(gender);
        dest.writeString(contact1);
        dest.writeString(contact2);
        dest.writeString(lastName);
        dest.writeString(firstName);
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        @Override
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        @Override
        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    @Nullable
    public Uri getProfilePic() {
        if (avatar != null) {
            return Uri.parse(ImageUtils.imageUrl(avatar));
        }
        return null;
    }

    @Nullable
    public String getFullName() {
        if (firstName == null || lastName == null) {
            return null;
        }
        return firstName + " " + lastName;
    }

    @Nullable
    public Long getDob() {
        return dateOfBirth == null ? null : parseDate(dateOfBirth);
    }



}
