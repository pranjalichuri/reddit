package com.reddit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ActiveModel extends NameModel implements Parcelable {

    @SerializedName("active")
    private Boolean active;

    protected ActiveModel(Parcel in) {
        super(in);
        byte tmpActive = in.readByte();
        active = tmpActive == 0 ? null : tmpActive == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (active == null ? 0 : active ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ActiveModel> CREATOR = new Creator<ActiveModel>() {
        @Override
        public ActiveModel createFromParcel(Parcel in) {
            return new ActiveModel(in);
        }

        @Override
        public ActiveModel[] newArray(int size) {
            return new ActiveModel[size];
        }
    };
}