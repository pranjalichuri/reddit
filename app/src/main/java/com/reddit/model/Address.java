package com.reddit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Address implements Parcelable {

    @SerializedName("area")
    public String area;

    @SerializedName("country")
    public String country;

    @SerializedName("address2")
    public String address2;

    @SerializedName("city")
    public String city;

    @SerializedName("address1")
    public String address1;

    @SerializedName("postalCode")
    public Integer postalCode;

    @SerializedName("state")
    public String state;

    @SerializedName("address_name")
    public String addressName;

    @SerializedName("id")
    public int id;

    protected Address(Parcel in) {
        area = in.readString();
        country = in.readString();
        address2 = in.readString();
        city = in.readString();
        address1 = in.readString();
        if (in.readByte() == 0) {
            postalCode = null;
        } else {
            postalCode = in.readInt();
        }
        state = in.readString();
        addressName = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(area);
        dest.writeString(country);
        dest.writeString(address2);
        dest.writeString(city);
        dest.writeString(address1);
        if (postalCode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(postalCode);
        }
        dest.writeString(state);
        dest.writeString(addressName);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public String toString() {
        return
                "Address{" +
                        "area = '" + area + '\'' +
                        ",country = '" + country + '\'' +
                        ",address2 = '" + address2 + '\'' +
                        ",city = '" + city + '\'' +
                        ",address1 = '" + address1 + '\'' +
                        ",postalCode = '" + postalCode + '\'' +
                        ",state = '" + state + '\'' +
                        ",address_name = '" + addressName + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}