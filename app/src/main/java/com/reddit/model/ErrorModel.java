package com.reddit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sa.zad.easyform.easyform.ObjectUtils;
import sa.zad.easyform.easyform.StringUtils;

public class ErrorModel {


    @SerializedName("error")
    public Error error;

    public static class Error {
        @SerializedName("status")
        public int status;
        @SerializedName("fieldss")
        public List<String> fields;
        @SerializedName("description")
        public String description;
        @SerializedName("error")
        public String error;
        @SerializedName("error_code")
        public Integer errorCode;
        @SerializedName("message")
        public String message;
    }

    public static String generateErrorMessage(ErrorModel errorModel) {
        if (ObjectUtils.isNotNull(errorModel.error.fields)) {
            StringBuilder fields = new StringBuilder();
            for (String missing_field : errorModel.error.fields) {
                fields.append(missing_field).append(", ");
            }
            if(errorModel.error.fields.size() > 1){
                return StringUtils.stripTrailingLeadingNewLines(fields.toString()) + " fields are missing";
            }else {
                return StringUtils.stripTrailingLeadingNewLines(fields.toString()) + " field is missing";
            }
        }
        return errorModel.error.description;
    }
}
