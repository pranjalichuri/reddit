package com.reddit.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.CallSuper;

import com.google.gson.annotations.SerializedName;

public class BaseModel implements Parcelable {
    @SerializedName("id")
    public int id;

    public BaseModel() {
    }

    protected BaseModel(Parcel in) {
        id = in.readInt();
    }

    @Override
    @CallSuper
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseModel> CREATOR = new Creator<BaseModel>() {
        @Override
        public BaseModel createFromParcel(Parcel in) {
            return new BaseModel(in);
        }

        @Override
        public BaseModel[] newArray(int size) {
            return new BaseModel[size];
        }
    };
}