package com.reddit.components.home;

import androidx.annotation.NonNull;
import androidx.navigation.NavOptions;

import com.reddit.ActivityResult;
import com.reddit.AppBaseFragment;
import com.reddit.CurrentUser;
import com.reddit.MainEnvironment;
import com.reddit.R;
import com.reddit.api.MainApi;

import javax.inject.Inject;

import io.reactivex.subjects.BehaviorSubject;


public abstract class HomeFragment<V extends HomeViewModel>
    extends AppBaseFragment<V> {

    @Inject
    public MainEnvironment mainEnvironment;

    protected MainApi api() {
        return mainEnvironment.api();
    }

    protected CurrentUser currentUser() {
        return mainEnvironment.currentUser();
    }

    @Override
    protected BehaviorSubject<ActivityResult<?>> getNavigationFragmentResult() {
        return mainEnvironment.navigationFragmentResult();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mainEnvironment.permissionManager().onRequestPermissionsResult(getActivity(),requestCode, permissions, grantResults);
    }

    protected void goToHome() {
        openClosableFragment(R.id.userAccountFragment, null, new NavOptions.Builder()
                .setPopUpTo(R.id.userAccountFragment, true)
                .build());
    }
}
