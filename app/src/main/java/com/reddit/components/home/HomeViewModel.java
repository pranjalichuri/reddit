package com.reddit.components.home;

import com.reddit.AppBaseViewModel;
import com.reddit.MainEnvironment;
import com.reddit.api.MainApi;

import sa.zad.easypermission.PermissionManager;

public abstract class HomeViewModel extends AppBaseViewModel {

    public final MainApi api;
    private final MainEnvironment environment;

    public HomeViewModel(MainEnvironment environment) {
        super(environment);
        api = environment.api();
        this.environment = environment;
    }

    public PermissionManager permissionManager(){
        return environment.permissionManager();
    }
}
