package com.reddit.components.home

import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reddit.AppBaseActivity
import com.reddit.MainEnvironment
import com.reddit.R
import com.reddit.api.MainApi
import com.reddit.utils.ViewUtils
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject


class HomeActivity : AppBaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    @Inject
    lateinit var mainApi: MainApi

    @Inject
    lateinit var mainEnvironment: MainEnvironment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        val findNavController = findNavController(R.id.host_fragment)
        bottomNavigationView.setupWithNavController(findNavController)
        setSupportActionBar(toolbar)
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.host_fragment) as NavHostFragment? ?: return

        val navController = host.navController
        val topLevelDestinationIds = setOf(R.id.userAccountFragment, R.id.feedFragment)
        appBarConfiguration = AppBarConfiguration(topLevelDestinationIds)

        setupActionBar(navController, appBarConfiguration)
        setupBottomNavMenu(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            //hide bottom nav bar
            ViewUtils.switchVisibility(
                bottom_nav_view,
                topLevelDestinationIds.contains(destination.id)
            )
        }
    }


    private fun setupBottomNavMenu(navController: NavController) {
        bottom_nav_view?.setupWithNavController(navController)
    }

    private fun setupActionBar(
        navController: NavController,
        appBarConfig: AppBarConfiguration
    ) {
        // This allows NavigationUI to decide what label to show in the action bar
        // By using appBarConfig, it will also determine whether to
        // show the up arrow or drawer menu icon
        setupActionBarWithNavController(navController, appBarConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Have the NavigationUI look for an action or destination matching the menu
        // item id and navigate there if found.
        // Otherwise, bubble up to the parent.
        return item.onNavDestinationSelected(findNavController(R.id.host_fragment))
                || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        // Allows NavigationUI to support proper up navigation or the drawer layout
        // drawer menu, depending on the situation
        return findNavController(R.id.host_fragment).navigateUp(appBarConfiguration)
    }

}
