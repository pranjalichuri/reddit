package com.reddit.components.home.user_account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reddit.R
import com.reddit.components.home.HomeFragment

class UserAccountFragment : HomeFragment<UserAccountViewModel>() {

    override fun getViewModelClassType(): Class<UserAccountViewModel> {
        return UserAccountViewModel::class.java
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return View.inflate(context, R.layout.fragment_home, null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
