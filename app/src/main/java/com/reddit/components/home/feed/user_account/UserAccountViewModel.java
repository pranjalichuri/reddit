package com.reddit.components.home.user_account;

import com.reddit.MainEnvironment;
import com.reddit.components.home.HomeViewModel;

import javax.inject.Inject;

public class UserAccountViewModel extends HomeViewModel {

    @Inject
    public UserAccountViewModel(MainEnvironment mainEnvironment) {
        super(mainEnvironment);
    }
}
