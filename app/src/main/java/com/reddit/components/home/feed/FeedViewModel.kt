package com.reddit.components.home.feed

import com.reddit.MainEnvironment
import com.reddit.components.home.HomeViewModel

import javax.inject.Inject

class FeedViewModel @Inject
constructor(mainEnvironment: MainEnvironment) : HomeViewModel(mainEnvironment)
