package com.reddit.components.home.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reddit.R
import com.reddit.components.home.HomeFragment

class FeedFragment : HomeFragment<FeedViewModel>() {

    override fun getViewModelClassType(): Class<FeedViewModel> {
        return FeedViewModel::class.java
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return View.inflate(context, R.layout.fragment_feed, null)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
