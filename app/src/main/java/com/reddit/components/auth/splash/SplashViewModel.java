package com.reddit.components.auth.splash;

import com.reddit.AuthEnvironment;
import com.reddit.components.auth.AuthViewModel;

import javax.inject.Inject;

public class SplashViewModel extends AuthViewModel {

    @Inject
    public SplashViewModel(AuthEnvironment authEnvironment) {
        super(authEnvironment);
    }
}
