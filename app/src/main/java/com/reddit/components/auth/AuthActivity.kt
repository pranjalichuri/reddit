package com.reddit.components.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import com.reddit.AppBaseActivity
import com.reddit.R

class AuthActivity : AppBaseActivity() {

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, AuthActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onSupportNavigateUp() = this.findNavController(R.id.auth_nav).navigateUp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}
