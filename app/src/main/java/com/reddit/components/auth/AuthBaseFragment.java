package com.reddit.components.auth;

import com.reddit.ActivityResult;
import com.reddit.AppBaseFragment;
import com.reddit.AuthEnvironment;
import com.reddit.CurrentUser;
import com.reddit.api.AuthApi;

import javax.inject.Inject;

import io.reactivex.subjects.BehaviorSubject;


public abstract class AuthBaseFragment<V extends AuthViewModel> extends AppBaseFragment<V> {

    @Inject
    public AuthEnvironment authEnvironment;

    protected AuthApi api() {
        return authEnvironment.api();
    }

    protected CurrentUser currentUser() {
        return authEnvironment.currentUser();
    }

    @Override
    protected BehaviorSubject<ActivityResult<?>> getNavigationFragmentResult() {
        return authEnvironment.navigationFragmentResult();
    }
}
