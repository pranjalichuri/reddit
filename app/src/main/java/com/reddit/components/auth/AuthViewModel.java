package com.reddit.components.auth;

import com.reddit.AppBaseViewModel;
import com.reddit.AuthEnvironment;
import com.reddit.api.AuthApi;

public abstract class AuthViewModel extends AppBaseViewModel {

    protected final AuthApi authApi;

    public AuthViewModel(AuthEnvironment authEnvironment) {
        super(authEnvironment);
        this.authApi = authEnvironment.api();
    }

}
