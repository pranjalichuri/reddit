package com.reddit.components;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.CallSuper;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import sa.zad.easyform.easyform.ObjectUtils;

public abstract class HundredDaysFragment<V extends HundredDaysViewModel> extends Fragment {

    public final String TAG;

    protected V viewModel;


    public HundredDaysFragment() {
        TAG = getClass().getSimpleName();
    }

    private V getFragmentViewModel(Class<V> viewModel) {
        return ViewModelProviders.of(this).get(viewModel);
    }

    protected abstract Class<V> getViewModelClassType();


    final protected void showLongToast(CharSequence text) {
        showToast(text, Toast.LENGTH_LONG);
    }

    final protected void showShortToast(Integer value) {
        showToast(String.valueOf(value), Toast.LENGTH_SHORT);
    }

    final protected void showShortToast(CharSequence text) {
        showToast(text, Toast.LENGTH_SHORT);
    }


    private void showToast(CharSequence text, int toastLength) {
        Toast.makeText(getContext(), text, toastLength).show();
    }

    public void log(int integer) {
        log(String.valueOf(integer));
    }

    public void log(String string) {
        Log.i(TAG, string);
    }


    public void finish() {
        Navigation.findNavController(getView()).popBackStack();
    }


    public void bindClick(View view, Consumer<Object> onNext) {
        bindClick(view, onNext, throwable -> {
        });
    }

    public void bindClick(View view, Consumer<Object> onNext, Consumer<Throwable> throwable) {
        bindClick(view, onNext, throwable, () -> {
        });
    }

    public void bindClick(View view, Consumer<Object> onNext, Consumer<Throwable> throwable,
                          Action onComplete) {
        bindClick(view).subscribe(onNext, throwable, onComplete);
    }

    public Observable<Object> bindClick(@NonNull View view) {
        return RxView.clicks(view)
                .throttleFirst(1000, TimeUnit.MILLISECONDS)
                .observeOn(getSchedulerProviderUI());
    }

    public Scheduler getSchedulerProviderUI() {
        return AndroidSchedulers.mainThread();
    }

    public Scheduler getSchedulerProviderIO() {
        return Schedulers.io();
    }

    @Override
    @CallSuper
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        log("onViewCreated " + ObjectUtils.coalesce(getTag(), "No-Tag"));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        log("onCreate " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onCreate(savedInstanceState);
        viewModel = getFragmentViewModel(getViewModelClassType());

    }

    @CallSuper
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        log("onCreateView " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    @CallSuper
    public void onStart() {
        log("onStart " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onStart();
    }

    @Override
    @CallSuper
    public void onStop() {
        log("onStop " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onStop();
    }

    @Override
    @CallSuper
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        log("onViewStateRestored " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onDestroy() {
        log("onDestroy " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onDestroy();
    }

    @Override
    @CallSuper
    public void onResume() {
        log("onResume " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onResume();
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        log("onDestroyView " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onDestroyView();
    }

    @Override
    @CallSuper
    public void onDetach() {
        log("onDetach " + ObjectUtils.coalesce(getTag(), "No-Tag"));
        super.onDetach();
    }

    protected LiveData<Integer> showAlertDialogOk(String title, String message, boolean cancelAble) {
        MutableLiveData<Integer> liveData = new MutableLiveData<>();
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancelAble)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> liveData.setValue(which))
                .show();
        return liveData;
    }

    protected LiveData<Integer> showAlertDialogOk(String title, String message) {
        return showAlertDialogOk(title, message, false);
    }

    protected LiveData<Integer> showAlertDialogYesNo(String title, String message, @DrawableRes int icon) {
        return showAlertDialogYesNo(title, message, icon, getString(android.R.string.yes), getString(android.R.string.no));
    }

    protected LiveData<Integer> showAlertDialogYesNo(String title, String message, @DrawableRes int icon, String positiveName, String negativeName) {
        MutableLiveData<Integer> liveData = new MutableLiveData<>();
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setIcon(icon)
                .setPositiveButton(positiveName, (dialog, which) -> liveData.setValue(which))
                .setNegativeButton(negativeName, (dialog, which) -> liveData.setValue(which))
                .show();
        return liveData;
    }
}
