package com.reddit

import android.app.Dialog
import android.content.Context
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.reddit.utils.ViewUtils

open abstract class BaseDialog(context: Context, @LayoutRes layoutRes: Int) : Dialog(context) {


    init {
        val inflate = ViewUtils.inflate(context, layoutRes)
        setContentView(inflate)
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}
