package com.reddit.calendar;

import org.joda.time.DateTime;

public interface DateSelectedListener{
        void selected(DateTime dateTime);
    }