package com.reddit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.reddit.utils.ImageUtils
import com.reddit.utils.ViewUtils
import kotlinx.android.synthetic.main.fragment_zoomable_image.*


class ZoomableImageFragment : FullScreenMediaFragment() {
    companion object {
        fun showDialog(fragmentManager: FragmentManager, url: String?, token: String? = null) {
            showDialog(ZoomableImageFragment(), fragmentManager, url, token)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_zoomable_image, container, false)
    }

    override fun showMedia(url: String, token: String?) {
        ImageUtils.displayImageWithResponse(zoomable_img, url, token) {
            ViewUtils.switchVisibility(loading_img_progress_bar, false)
        }
        close.setOnClickListener {
            dismiss()
        }
    }

}
