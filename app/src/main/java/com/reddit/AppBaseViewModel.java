package com.reddit;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.common.util.ArrayUtils;
import com.reddit.model.DataModel;
import com.reddit.model.ErrorModel;
import com.reddit.model.IntPageDataModel;
import com.reddit.model.PageDataModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;
import rx.functions.Action1;
import sa.zad.easyform.easyform.ObjectUtils;
import sa.zad.easyretrofit.observables.NeverErrorObservable;

public abstract class AppBaseViewModel extends ViewModel {

    public final CurrentUser currentUser;
    private final BehaviorSubject<ActivityResult<?>> activityResult;
    private CompositeDisposable disposable;
    private MutableLiveData<LoadingData> loadingLiveData = new MutableLiveData<>();
    private MutableLiveData<ErrorData> errorLiveData = new MutableLiveData<>();
    private MutableLiveData<ApiErrorData> apiErrorLiveData = new MutableLiveData<>();
    protected BehaviorSubject<NotifyOnce<?>> notification;

    public AppBaseViewModel(Environment environment) {
        this.currentUser = environment.currentUser();
        this.activityResult = environment.navigationFragmentResult();
        this.notification = environment.getNotification();
    }

    public final <T extends BaseNotificationModel> LiveData<T> notificationListener(String[] notificationType) {
        MutableLiveData<T> notificationModelMutableLiveData = new MutableLiveData<>();
        disposable.add(notification
                .filter(notifyOnce -> ArrayUtils.contains(notificationType, notifyOnce.getType()))
                .filter(notifyOnce -> !notifyOnce.isRead())
                .observeOn(AndroidSchedulers.mainThread())
                .map(notifyOnce -> (T) notifyOnce.getNotificationModel())
                .subscribe(notificationModelMutableLiveData::setValue,
                        throwable -> {
                        }));

        return notificationModelMutableLiveData;
    }

    public final <T extends BaseNotificationModel> LiveData<T> notificationListener(String notificationType) {
        String[] types = {notificationType};
        return notificationListener(types);
    }

    protected <M> Observable<M> observable(NeverErrorObservable<DataModel<M>> observable, int requestId) {
        shootLoading(true, requestId);
        return observable
                .apiException(errorModel ->
                        shootError(errorModel, requestId), ErrorModel.class)
                .exception(throwable -> shootError(throwable, requestId))
                .doFinally(() -> shootLoading(false, requestId)).map(mDataModel -> mDataModel.data);
    }

    protected <M> LiveData<PageDataModel<Integer,M>> pagedLiveData(NeverErrorObservable<IntPageDataModel<M>> observable, @NonNull Action1<PageDataModel<Integer, M>> callback, Action1<Throwable> errorCallback, int requestId) {
        MutableLiveData<PageDataModel<Integer,M>> mutableLiveData = new MutableLiveData<>();
        shootLoading(true, requestId);
        observable
                .successResponse(dataModelResponse -> {
                    final PageDataModel<Integer, M> data = dataModelResponse.body();
                    callback.call(data);
                    mutableLiveData.setValue(data);
                })
                .apiException(errorModel ->
                        shootError(errorModel, requestId), ErrorModel.class)
                .exception(throwable -> shootError(throwable, requestId))
                .doFinally(() -> shootLoading(false, requestId))
                .subscribe();
        return mutableLiveData;
    }

    public LiveData<Void> liveDataNoResponse(NeverErrorObservable<Void> observable, int requestId) {
        MutableLiveData<Void> mutableLiveData = new MutableLiveData<>();
        shootLoading(true, requestId);
        observable
                .successResponse(dataModelResponse -> mutableLiveData.setValue(null))
                .apiException(errorModel ->
                        shootError(errorModel, requestId), ErrorModel.class)
                .exception(throwable -> shootError(throwable, requestId))
                .doFinally(() -> shootLoading(false, requestId))
                .subscribe();
        return mutableLiveData;
    }
    public  <M> LiveData<M> livedata(NeverErrorObservable<DataModel<M>> observable, int requestId) {
        MutableLiveData<M> mutableLiveData = new MutableLiveData<>();
        shootLoading(true, requestId);
        observable
                .successResponse(dataModelResponse -> mutableLiveData.setValue(dataModelResponse.body().data))
                .apiException(errorModel ->
                        shootError(errorModel, requestId), ErrorModel.class)
                .exception(throwable -> shootError(throwable, requestId))
                .doFinally(() -> shootLoading(false, requestId))
                .subscribe();
        return mutableLiveData;
    }


    public final Observable<ActivityResult<?>> activityResult(int requestCode) {
        return activityResult
                .filter(ActivityResult::isOk)
                .filter(result -> result.isRequestCode(requestCode))
                .doOnNext(result -> activityResult.onNext(new ActivityResult<>(3321235, 1, null)));
    }

    public final <V> Observable<V> activityResult(int requestCode, Class<V> cast) {
        return activityResult(requestCode)
                .map(ActivityResult::getValue)
                .cast(cast);
    }

    public final MutableLiveData<ActivityResult<?>> onNavigationResult(int requestCode) {
        MutableLiveData<ActivityResult<?>> activityResultLiveData = new MutableLiveData<>();
        disposable.add(activityResult(requestCode)
                .subscribe(activityResultLiveData::setValue, throwable -> {
                }));
        return activityResultLiveData;
    }

    public final <V> MutableLiveData<V> onNavigationResult(int requestCode, Class<V> cast) {
        MutableLiveData<V> activityResultLiveData = new MutableLiveData<>();
        disposable.add(activityResult(requestCode, cast)
                .subscribe(activityResultLiveData::setValue, throwable -> {
                }));
        return activityResultLiveData;
    }

    @CallSuper
    @Override
    protected void onCleared() {
        //Todo temp fix can't locate bug
        if(ObjectUtils.isNotNull(disposable)) {
            disposable.dispose();
        }
        super.onCleared();
    }

    @CallSuper
    public void onDestroyView() {
        disposable.dispose();
    }

    public final LiveData<ErrorData> errorLiveData() {
        return errorLiveData;
    }

    public MutableLiveData<LoadingData> loadingLiveData() {
        return loadingLiveData;
    }

    public MutableLiveData<ApiErrorData> apiErrorLiveData() {
        return apiErrorLiveData;
    }

    @CallSuper
    public void onViewCreated() {
        disposable = new CompositeDisposable();
    }

    protected void shootError(ErrorModel errorModel, int id) {
        final ApiErrorData value = new ApiErrorData(new ApiErrorException(errorModel), id);
        new Handler(Looper.getMainLooper()).post(() -> {
            apiErrorLiveData.setValue(value);
            value.discard();
        });
    }

    protected void shootError(Throwable throwable, int id) {
        final ErrorData value = new ErrorData(throwable, id);
        new Handler(Looper.getMainLooper()).post(() -> {
            errorLiveData.setValue(value);
            value.discard();
        });
    }

    protected void shootLoading(boolean loading, int id) {
        final LoadingData value = new LoadingData(loading, id);
        new Handler(Looper.getMainLooper()).post(() -> {
            loadingLiveData.setValue(value);
            value.discard();
        });
    }

    public abstract static class LiveDataStatus {
        private int id;

        public LiveDataStatus(int id) {
            this.id = id;
        }

        public boolean isThisRequest(int id) {
            return this.id == id;
        }

        public int getId() {
            return id;
        }

        public void discard() {
            id = 3412341;
        }

    }

    public static class ApiErrorData extends LiveDataStatus {
        private final ApiErrorException apiErrorException;

        public ApiErrorData(ApiErrorException apiErrorException, int id) {
            super(id);
            this.apiErrorException = apiErrorException;
        }

        public ApiErrorException getApiErrorException() {
            return apiErrorException;
        }
    }

    public static class ErrorData extends LiveDataStatus {
        private final Throwable throwable;

        public ErrorData(Throwable throwable, int id) {
            super(id);
            this.throwable = throwable;
        }

        public Throwable getThrowable() {
            return throwable;
        }
    }

    public static class LoadingData extends LiveDataStatus {
        private final boolean isLoading;

        public LoadingData(boolean isLoading, int id) {
            super(id);
            this.isLoading = isLoading;
        }

        public boolean isLoading() {
            return isLoading;
        }
    }
    public static class ApiErrorException extends Exception{

        private final ErrorModel errorModel;

        public ApiErrorException(ErrorModel errorModel) {
            super(new Throwable(errorModel.error.message));
            this.errorModel = errorModel;
        }

        public ErrorModel getErrorModel() {
            return errorModel;
        }

        public ErrorModel.Error getError(){
            return getErrorModel().error;
        }
    }
}
