package com.reddit;

import com.reddit.api.MainApi;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.subjects.BehaviorSubject;
import sa.zad.easypermission.PermissionManager;

@Singleton
public class MainEnvironment extends Environment {

    private final MainApi mainApi;
    private final PermissionManager permissionManager;

    @Inject
    public MainEnvironment(FCMToken fcmToken, MainApi authApi, BehaviorSubject<ActivityResult<?>> behaviorSubject, CurrentUser currentUser, PermissionManager permissionManager, @Named("notification") BehaviorSubject<NotifyOnce<?>> notifyOnceBehaviorSubject) {
        super(fcmToken, behaviorSubject, currentUser, notifyOnceBehaviorSubject);
        this.mainApi = authApi;
        this.permissionManager = permissionManager;
    }

    @NotNull
    public MainApi api() {
        return mainApi;
    }

    @NotNull
    public PermissionManager permissionManager() {
        return permissionManager;
    }

}
