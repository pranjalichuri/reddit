package com.reddit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public abstract class AppBaseActivity extends DaggerAppCompatActivity {

    protected CompositeDisposable compositeDisposable;

    public final String TAG;

    @Inject
    Environment environment;

    public AppBaseActivity() {
        TAG = getClass().getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    public void bindClick(View view, Consumer<Object> onNext) {
        bindClick(view, onNext, throwable -> {
        });
    }

    public void bindClick(View view, Consumer<Object> onNext, Consumer<Throwable> throwable) {
        bindClick(view, onNext, throwable, () -> {
        });
    }

    public void bindClick(View view, Consumer<Object> onNext, Consumer<Throwable> throwable,
                          Action onComplete) {
        bindClick(view).subscribe(onNext, throwable, onComplete);
    }

    public Observable<Object> bindClick(@NonNull View view) {
        return RxView.clicks(view)
                .throttleFirst(1000, TimeUnit.MILLISECONDS)
                .observeOn(getSchedulerProviderUI());
    }
    public Scheduler getSchedulerProviderUI() {
        return AndroidSchedulers.mainThread();
    }

    public Scheduler getSchedulerProviderIO() {
        return Schedulers.io();
    }

    final protected void showLongToast(CharSequence text) {
        showToast(text, Toast.LENGTH_LONG);
    }

    final protected void showShortToast(Integer value) {
        showToast(String.valueOf(value), Toast.LENGTH_SHORT);
    }

    final protected void showShortToast(CharSequence text) {
        showToast(text, Toast.LENGTH_SHORT);
    }


    private void showToast(CharSequence text, int toastLength) {
        Toast.makeText(this, text, toastLength).show();
    }

    public void log(int integer) {
        log(String.valueOf(integer));
    }

    public void log(String string) {
        Log.i(TAG, string);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        environment.navigationFragmentResult().onNext(new ActivityResult<>(requestCode, resultCode, data));
    }
}
