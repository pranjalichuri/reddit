package com.reddit.service;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.reddit.ActivityResult;
import com.reddit.CurrentUser;
import com.reddit.FCMToken;
import com.reddit.NotifyOnce;
import com.reddit.api.BackgroundApi;
import com.reddit.model.FCMTokenBody;
import com.reddit.model.UserInfo;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.AndroidInjection;
import io.reactivex.subjects.BehaviorSubject;
import sa.zad.easyform.easyform.ObjectUtils;


public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Inject
    FCMToken fcmToken;

    @Inject
    CurrentUser currentUser;

    @Inject
    BackgroundApi backgroundApi;

    @Inject
    BehaviorSubject<ActivityResult<?>> behaviorSubject;

    @Inject
    @Named("notification")
    BehaviorSubject<NotifyOnce<?>> notification;

    @Inject
    Gson gson;

    public static void updateToken(String token, FCMToken fcmToken, CurrentUser currentUser, @Nullable BackgroundApi backgroundApi) {
        fcmToken.putToken(token);
        final UserInfo user = currentUser.getUser();
        if (ObjectUtils.isNotNull(user) && ObjectUtils.isNotNull(backgroundApi)) {
            backgroundApi
                    .fcmRegister(new FCMTokenBody("dasf","1", token))
                    .exception(throwable -> {
                        Log.d("Fcm_token", "Error updating token " + throwable.getMessage());
                    })
                    .subscribe(dataModel -> {
                        Log.d("Fcm_token", "onNewToken Update " + dataModel.data.name);
                    });
        }
    }

    public static void requestAndUpdate(FCMToken fcmToken, CurrentUser currentUser, BackgroundApi backgroundApi) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(
                task -> {
                    if (task.isSuccessful() && ObjectUtils.isNotNull(task.getResult())) {
                        final String token = task.getResult().getToken();
                        Log.d("Fcm_token", "requestFCMToken Received " + token);
                        updateToken(token, fcmToken, currentUser, backgroundApi);
                    }
                });
    }

    @WorkerThread
    public static void deleteFcmToken() throws IOException {
        try {
            FirebaseInstanceId.getInstance().deleteInstanceId();
            Log.d("Fcm_token", "Deleted");
        } catch (IOException e) {
            Log.d("Fcm_token", "Fail to delete " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidInjection.inject(this);
    }

    @Override
    public void onNewToken(String s) {
        Log.d("Fcm_token", "onNewToken Received " + s);
        updateToken(s, fcmToken, currentUser, backgroundApi);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("MessageReceived", remoteMessage.getMessageId());
        final RemoteMessage.Notification notification = remoteMessage.getNotification();
        if(notification == null) {
            final Map<String, String> data = remoteMessage.getData();
            Log.d("MessageReceived-Data", data.toString());
            final JSONObject jsonObject = new JSONObject(data);
            String notificationType = data.get("type");
            final String json = jsonObject.toString();
        } else {
            NotificationHelper.createNotification(notification);
        }
    }
}
