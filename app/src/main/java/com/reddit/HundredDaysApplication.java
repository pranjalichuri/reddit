package com.reddit;

import com.facebook.stetho.Stetho;
import com.reddit.api.BackgroundApi;
import com.reddit.di.DaggerAppComponent;

import net.danlew.android.joda.JodaTimeAndroid;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class HundredDaysApplication extends MultiDexApplication {

    private static HundredDaysApplication INSTANCE;


    public HundredDaysApplication() {
        INSTANCE = this;
    }

    @Inject
    public CurrentUser currentUser;

    @Inject
    public FCMToken fcmToken;

    @Inject
    public BackgroundApi backgroundApi;

    @Override
    public void onCreate() {
        super.onCreate();

        JodaTimeAndroid.init(this);

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .build());

        /*currentUser.loggedInUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userInfo -> {
                    AppFirebaseMessagingService.requestAndUpdate(fcmToken, currentUser, backgroundApi);
                });

        currentUser.loggedOutUser()
                .subscribeOn(Schedulers.io())
                .subscribe(__ -> AppFirebaseMessagingService.deleteFcmToken());*/
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    public static HundredDaysApplication getInstance() {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        return new HundredDaysApplication();
    }
}
