package com.reddit;

import com.reddit.api.AuthApi;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import io.reactivex.subjects.BehaviorSubject;

@Singleton
public class AuthEnvironment extends Environment {

    private final AuthApi authApi;

    @Inject
    public AuthEnvironment(FCMToken fcmToken, AuthApi authApi, BehaviorSubject<ActivityResult<?>> behaviorSubject, CurrentUser currentUser, @Named("notification") BehaviorSubject<NotifyOnce<?>> notifyOnceBehaviorSubject) {
        super(fcmToken, behaviorSubject, currentUser, notifyOnceBehaviorSubject);
        this.authApi = authApi;
    }

    @NotNull
    public AuthApi api() {
        return authApi;
    }
}
