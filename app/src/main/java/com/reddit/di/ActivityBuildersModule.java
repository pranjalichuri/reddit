package com.reddit.di;

import com.reddit.components.auth.AuthActivity;
import com.reddit.components.home.HomeActivity;
import com.reddit.di.auth.AuthFragmentBuildersModule;
import com.reddit.di.auth.AuthModule;
import com.reddit.di.auth.AuthScope;
import com.reddit.di.auth.AuthViewModelsModule;
import com.reddit.di.main.MainFragmentBuildersModule;
import com.reddit.di.main.MainModule;
import com.reddit.di.main.MainScope;
import com.reddit.di.main.MainViewModelsModule;
import com.reddit.service.AppFirebaseMessagingService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    @AuthScope
    @ContributesAndroidInjector(
            modules = {AuthFragmentBuildersModule.class, AuthViewModelsModule.class, AuthModule.class})
    abstract AuthActivity authActivity();

    @MainScope
    @ContributesAndroidInjector(
            modules = {MainFragmentBuildersModule.class, MainViewModelsModule.class, MainModule.class}
    )
    abstract HomeActivity homeActivity();

    @ContributesAndroidInjector
    abstract AppFirebaseMessagingService appFirebaseMessagingService();
}
