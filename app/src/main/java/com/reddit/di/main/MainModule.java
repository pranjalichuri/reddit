package com.reddit.di.main;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.reddit.ActivityResult;
import com.reddit.AppEasyRetrofit;
import com.reddit.CurrentUser;
import com.reddit.FCMToken;
import com.reddit.MainEnvironment;
import com.reddit.NotifyOnce;
import com.reddit.ObjectPreference;
import com.reddit.api.MainApi;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.subjects.BehaviorSubject;
import sa.zad.easypermission.PermissionManager;

@Module
public class MainModule {

    @MainScope
    @Provides
    static MainEnvironment providesAuthEnvironment(FCMToken fcmToken, MainApi mainApi, PermissionManager permissionManager, CurrentUser currentUser, BehaviorSubject<ActivityResult<?>> navigationFragmentResult, @Named("notification") BehaviorSubject<NotifyOnce<?>> notifyOnceBehaviorSubject) {
        return new MainEnvironment(fcmToken, mainApi, navigationFragmentResult, currentUser, permissionManager, notifyOnceBehaviorSubject);
    }

    @MainScope
    @Provides
    static MainApi providesMainApi(AppEasyRetrofit appEasyRetrofit) {
        return appEasyRetrofit.provideRetrofit().create(MainApi.class);
    }

    @MainScope
    @Provides
    static ObjectPreference providesAddPharmacyDraft(SharedPreferences sharedPreferences, Gson gson) {
        return new ObjectPreference(sharedPreferences, gson);
    }

}

