package com.reddit.di.main;


import com.reddit.components.home.feed.FeedFragment;
import com.reddit.components.home.user_account.UserAccountFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract FeedFragment feedFragment();

    @ContributesAndroidInjector
    abstract UserAccountFragment userAccountFragment();
}
