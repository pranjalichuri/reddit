package com.reddit.di.main;

import androidx.lifecycle.ViewModel;

import com.reddit.components.home.feed.FeedViewModel;
import com.reddit.components.home.user_account.UserAccountViewModel;
import com.reddit.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserAccountViewModel.class)
    public abstract ViewModel userAccountViewModel(UserAccountViewModel userAccountViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FeedViewModel.class)
    public abstract ViewModel feedViewModel(FeedViewModel feedViewModel);
}




