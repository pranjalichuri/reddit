package com.reddit.di;

import android.Manifest;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reddit.ActivityResult;
import com.reddit.AppEasyRetrofit;
import com.reddit.CurrentUser;
import com.reddit.Environment;
import com.reddit.FCMToken;
import com.reddit.NotifyOnce;
import com.reddit.api.BackgroundApi;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.subjects.BehaviorSubject;
import sa.zad.easypermission.AppPermission;
import sa.zad.easypermission.AppPermissionImp;
import sa.zad.easypermission.PermissionManager;

@Module
public class AppModule {

    public static final int STORAGE_PERMISSION_REQUEST_CODE = 100;
    private static final int MAX_REQUEST = 2;

    @Provides
    @Singleton
    PermissionManager providesPermissionManager(SharedPreferences sharedPreferences) {
        return new PermissionManager(storagePermission(sharedPreferences));
    }

    private AppPermission storagePermission(SharedPreferences sharedPreferences) {
        return new AppPermissionImp(STORAGE_PERMISSION_REQUEST_CODE, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
        }, MAX_REQUEST, sharedPreferences);
    }

    @Singleton
    @Provides
    static FCMToken providesFCMToken(SharedPreferences sharedPreferences, Gson gson) {
        return new FCMToken(sharedPreferences, gson);
    }

    @Singleton
    @Provides
    static AppEasyRetrofit providesAppEasyRetrofit(Application application, CurrentUser currentUser, Gson gson) {
        return new AppEasyRetrofit(application, currentUser, gson);
    }

    @Singleton
    @Provides
    static SharedPreferences providesSharedPreferences(Application application) {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Singleton
    @Provides
    static CurrentUser providesCurrentUser(SharedPreferences sharedPreferences, Gson gson) {
        return new CurrentUser(sharedPreferences, gson);
    }

    @Singleton
    @Provides
    static Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Singleton
    @Provides
    static BehaviorSubject<ActivityResult<?>> provideFragmentNavigationResult() {
        return BehaviorSubject.create();
    }

    @Singleton
    @Provides
    @Named("notification")
    static BehaviorSubject<NotifyOnce<?>> notificationResultBehaviorSubject() {
        return BehaviorSubject.create();
    }

    @Singleton
    @Provides
    static Environment providesEnvironment(FCMToken fcmToken, CurrentUser currentUser, BehaviorSubject<ActivityResult<?>> navigationFragmentResult, @Named("notification") BehaviorSubject<NotifyOnce<?>> notifyOnceBehaviorSubject) {
        return new Environment(fcmToken, navigationFragmentResult, currentUser, notifyOnceBehaviorSubject);
    }

    @Singleton
    @Provides
    static BackgroundApi providesBackgroundApi(AppEasyRetrofit appEasyRetrofit){
        return appEasyRetrofit.provideRetrofit().create(BackgroundApi.class);
    }
}
















