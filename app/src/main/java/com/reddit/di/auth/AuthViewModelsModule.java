package com.reddit.di.auth;

import androidx.lifecycle.ViewModel;

import com.reddit.components.auth.splash.SplashViewModel;
import com.reddit.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class AuthViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    public abstract ViewModel splashViewModel(SplashViewModel splashViewModel);

}
