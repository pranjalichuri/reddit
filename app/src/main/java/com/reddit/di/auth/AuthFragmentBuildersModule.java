package com.reddit.di.auth;


import com.reddit.components.auth.splash.SplashFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AuthFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract SplashFragment splashFragment();


}
