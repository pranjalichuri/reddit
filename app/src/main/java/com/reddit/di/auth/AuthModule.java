package com.reddit.di.auth;

import com.reddit.ActivityResult;
import com.reddit.AppEasyRetrofit;
import com.reddit.AuthEnvironment;
import com.reddit.CurrentUser;
import com.reddit.FCMToken;
import com.reddit.NotifyOnce;
import com.reddit.api.AuthApi;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.subjects.BehaviorSubject;

@Module
public class AuthModule {


    @AuthScope
    @Provides
    static AuthEnvironment providesAuthEnvironment(FCMToken fcmToken, AuthApi authApi, CurrentUser currentUser, BehaviorSubject<ActivityResult<?>> navigationFragmentResult, @Named("notification") BehaviorSubject<NotifyOnce<?>> notifyOnceBehaviorSubject){
        return new AuthEnvironment(fcmToken, authApi, navigationFragmentResult, currentUser, notifyOnceBehaviorSubject);
    }

    @AuthScope
    @Provides
    static AuthApi providesService(AppEasyRetrofit easyRetrofit){
        return easyRetrofit.provideRetrofit().create(AuthApi.class);
    }
}
