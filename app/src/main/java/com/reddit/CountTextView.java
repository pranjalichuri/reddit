package com.reddit;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import sa.zad.easyform.easyform.ObjectUtils;

public class CountTextView extends AppCompatTextView {

    private static final int[] DEFAULT_COUNT = {R.attr.defaultCount};
    private static final int[] COUNT_MODIFIED = {R.attr.countModified};

    private boolean defaultCount;
    private boolean countModified;

    public CountTextView(Context context) {
        super(context);
    }

    public CountTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CountTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (defaultCount) {
            mergeDrawableStates(drawableState, DEFAULT_COUNT);
        } else if (countModified) {
            mergeDrawableStates(drawableState, COUNT_MODIFIED);
        }
        return drawableState;
    }

    public void isModified(Integer count, int defaultCount){
        if(ObjectUtils.isNull(count)){
            defaultCount();
        }else {
            countModified();
        }
    }

    public void countModified() {
        setAllToFalse();
        countModified = true;
        refreshDrawableState();
    }

    public void defaultCount() {
        setAllToFalse();
        defaultCount = true;
        refreshDrawableState();
    }

    public void toDefault() {
        setAllToFalse();
        refreshDrawableState();
    }


    private void setAllToFalse() {
        countModified = false;
        defaultCount = false;
    }
}
