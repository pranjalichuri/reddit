package com.reddit.api;

import com.reddit.model.DataModel;
import com.reddit.model.FCMTokenBody;
import com.reddit.model.NameModel;

import retrofit2.http.Body;
import retrofit2.http.POST;
import sa.zad.easyretrofit.observables.NeverErrorObservable;

public interface BackgroundApi {
    @POST("fcm_register/")
    NeverErrorObservable<DataModel<NameModel>> fcmRegister(@Body FCMTokenBody fcmTokenBody);

}
