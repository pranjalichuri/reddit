package com.reddit;

import android.app.Application;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sa.zad.easyretrofit.EasyRetrofit;
import sa.zad.easyretrofit.EasyRetrofitClient;

public class AppEasyRetrofit extends EasyRetrofit {

  private final CurrentUser currentUser;
  private final Gson gson;

  public AppEasyRetrofit(Application application, CurrentUser currentUser, Gson gson) {
    super(application);
    this.currentUser = currentUser;
    this.gson = gson;
  }

  @NonNull
  @Override
  public Retrofit.Builder retrofitBuilderReady(@NonNull Retrofit.Builder retrofitBuilder) {
    return retrofitBuilder
        .baseUrl(BuildConfig.DOMAIN_URL);
  }

  @NonNull
  @Override
  protected Converter.Factory addConverterFactory() {
    return GsonConverterFactory.create(gson);
  }

  @NonNull
  @Override
  protected EasyRetrofitClient easyRetrofitClient() {
    return new AppEasyRetrofitClient(provideApplication(), currentUser);
  }

}
