package com.reddit.utils;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sa.zad.easyform.easyform.Func1;
import sa.zad.easyform.easyform.ObjectUtils;


public class Utils {

    public static final String APP_DATE_FORMATTER = "yyyy-MM-dd";
    public static final String APP_TIME_FORMATTER = "h:mm:ss";
    public static final String APP_TIME_FORMATTER_24_HOURS = "H:mm:ss";

    public boolean isTimeAvailable(DateTime dateTime){
        return dateTime.getMillisOfDay() != 0;
    }

    @NonNull
    public static DateTime parseTime(@NonNull String time) {
        final String[] split = time.split(":");
        DateTime dateTime = new DateTime().withTimeAtStartOfDay();
        dateTime = dateTime
                .plusHours(Integer.parseInt(split[0]))
                .plusMinutes(Integer.parseInt(split[1]))
                .plusSeconds(Integer.parseInt(split[2]));

        final String s = dateTime.toString();
        return dateTime;
    }

    @Nullable
    public static String parseDateLong(@Nullable Long date) {
        return parseDateLong(date, APP_DATE_FORMATTER);
    }

    @Nullable
    public static String parseDateLong(@Nullable Long date, String formatter) {
        if (date != null) {
            DateTime dateTime = new DateTime(date);
            return dateTime.toString(formatter);
        }
        return null;
    }

    @NonNull
    public static DateTime parseDateToDateTime(@NonNull String date) {
        return DateTime.parse(date);
    }

    @NonNull
    public static DateTime parseDateTime(@NonNull String date) {
        return parseDateToDateTime(date);
    }

    @NonNull
    public static Long parseDate(@NonNull String date) {
        return parseDateToDateTime(date).getMillis();
    }

    public static boolean isSameDay(@NonNull String date, @NonNull String date1) {
        return isSameDay(parseDate(date), parseDate(date1));
    }

    public static boolean isSameDay(@Nullable Long date, @Nullable Long date1) {
        if (ObjectUtils.isNull(date) || ObjectUtils.isNull(date1)) {
            return false;
        }
        DateTime dateTime = new DateTime(date);
        DateTime dateTime1 = new DateTime(date1);

        return isSameDay(dateTime, dateTime1);
    }

    public static boolean isDateInBetween(@Nullable String startDate, @Nullable String endDate, @NonNull Long date) {
        if (ObjectUtils.isNull(startDate) && ObjectUtils.isNull(endDate)) {
            return false;
        }

        if (ObjectUtils.isNull(startDate)) {
            return isSameDay(parseDate(endDate), date);
        }

        if (ObjectUtils.isNull(endDate)) {
            return isSameDay(parseDate(startDate), date);
        }

        final Long startTime = parseDate(startDate);
        final Long endTime = parseDate(endDate);

        return startTime <= date && endTime >= date;
    }

    public static boolean isSameDay(@NonNull DateTime date, @NonNull DateTime date1) {
        return DateTime.parse(date.toString(APP_DATE_FORMATTER)).getMillis() == DateTime.parse(date1.toString(APP_DATE_FORMATTER)).getMillis();
    }

    public static boolean isToday(DateTime time) {
        return LocalDate.now().compareTo(new LocalDate(time)) == 0;
    }

    public static boolean isTomorrow(DateTime time) {
        return LocalDate.now().plusDays(1).compareTo(new LocalDate(time)) == 0;
    }

    public static boolean isYesterday(DateTime time) {
        return LocalDate.now().minusDays(1).compareTo(new LocalDate(time)) == 0;
    }

    public static String ordinal(int i) {
        String[] suffixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + suffixes[i % 10];

        }
    }

    @Nullable
    public static <T extends Fragment> T createInstance(Class<T> tClass) {
        try {
            final T t = tClass.newInstance();
            Bundle args = new Bundle();
            t.setArguments(args);
            return t;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static <I> I compareFindItem(List<I> list, Func1<I, Boolean> callback) {
        for (int i = 0; i < list.size(); i++) {
            final I t = list.get(i);
            if (callback.call(t)) {
                return t;
            }
        }
        return null;
    }

    public static <I> int compareFindIndex(List<I> list, Func1<I, Boolean> callback) {
        for (int i = 0; i < list.size(); i++) {
            if (callback.call(list.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public static <E, V> List<V> extractValue(@NonNull final List<E> list,
                                              @NonNull final Func1<E, V> callback) {
        List<V> returnList = new ArrayList<>();
        for (E e : list) {
            final V r = callback.call(e);
            if (ObjectUtils.isNotNull(r)) {
                returnList.add(r);
            }
        }
        return returnList;
    }



    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoPath, new HashMap<>());
            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static Float decimalN(float value, int n) {
        return decimalN(value, n, RoundingMode.CEILING);
    }

    public static Float decimalN(float value, int n, RoundingMode roundingMode) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; i++) {
            stringBuilder.append("#");
        }
        final DecimalFormat decimalFormat = new DecimalFormat("#." + stringBuilder.toString());
        decimalFormat.setRoundingMode(roundingMode);
        return Float.valueOf(decimalFormat.format(value));
    }

    public static String removeTrailingZeros(float value) {
        return decimalN(value, 2).toString().replaceAll("\\.?0*$", "");
    }
}
